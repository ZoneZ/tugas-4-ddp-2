package memorygame.exceptions;

import java.io.File;

/**
 * This class represents a custom exception in which it occurs
 * when the program is trying to find an image file.
 *
 * @author Farhan Azyumardhi Azmi - 1706979234
 */
public class ImageNotFoundException extends RuntimeException {

    /**
     * Class constructor.
     *
     * @param message : message to be written to the exception.
     */
    public ImageNotFoundException(String message) {
        super(message);
    }
}
