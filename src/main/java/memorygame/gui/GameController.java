package memorygame.gui;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import memorygame.component.AnimalCard;

/**
 * This class represents the mechanic of the game.
 *
 * @author Farhan Azyumardhi Azmi - 1706979234
 */
public class GameController {

    /**
     * A GUI window that is bound with this class instance.
     */
    private GameWindow mainWindow;

    /**
     * The number of remaining cards.
     */
    private int cardsRemaining;

    /**
     * The number of how many card the user has selected.
     */
    private int choiceNumber;

    /**
     * How many number of attempts the user has taken.
     */
    private int numOfTries;

    /**
     * ArrayList that stores selected cards.
     */
    private ArrayList<AnimalCard> chosenCards;

    /**
     * Status of the game.
     */
    private boolean gameOver;

    /**
     * Timer to create the "delay" when the user selects a card.
     */
    private Timer displayTimer = new Timer(250, lambda -> displayTimerAction());

    /**
     * Class constructor.
     *
     * @param mainWindow : GameWindow object as GUI Window.
     */
    public GameController(GameWindow mainWindow) {
        this.mainWindow = mainWindow;
        this.chosenCards = new ArrayList<>();
        for (AnimalCard card : this.mainWindow.getCards()) {
            card.setClickable(false);
        }
    }

    /**
     * Sets the function of start/stop button, exit button, and every cards.
     */
    public void run() {
        this.mainWindow.setStartStopButtonFunction(lambda -> this.startStopButtonPressed());
        this.mainWindow.setExitButtonFunction(lambda -> this.exitButtonPressed());
        for (AnimalCard card : this.mainWindow.getCards()) {
            card.setAnimalCardFunction(lambda -> this.animalCardButtonPressed(card));
        }
    }

    /**
     * The method that runs when the start/stop button is pressed.
     */
    public void startStopButtonPressed() {
        // Starts new game.
        if (this.mainWindow.getStartStopButton().getText().equals("Start Game")) {
            this.mainWindow.getStartStopButton().setText("Stop Game");
            this.mainWindow.getStatusLabel().setText("Pick a Card");
            // Sets the number of tries to 0.
            this.numOfTries = 0;
            this.mainWindow.getTriesNumberLabel().setText(String.valueOf(this.numOfTries));
            // Sets the number of remaining cards to 36.
            this.cardsRemaining = GameWindow.getNumOfTotalCards();
            // Choice number = 1 means that the user is going to select the first card.
            this.choiceNumber = 1;
            this.mainWindow.getExitButton().setEnabled(false);
            for (AnimalCard card : this.mainWindow.getCards()) {
                card.setClickable(true);
                card.setIcon(AnimalCard.getCover());
                card.setVisible(true);
            }
            this.gameOver = false;
        } else {
            // Stops the game, then shuffles all cards placements.
            Collections.shuffle(this.mainWindow.getCards());
            // Create new cards panel.
            this.mainWindow.setCardsPanel(this.mainWindow.createCardsPanel());
            for (AnimalCard card : this.mainWindow.getCards()) {
                card.setClickable(false);
                card.setVisible(true);
            }
            this.mainWindow.getStartStopButton().setText("Start Game");
            this.mainWindow.getExitButton().setEnabled(true);
            if (!this.gameOver) {
                this.mainWindow.getStatusLabel().setText("Game Stopped");
            }
        }
    }

    /**
     * Exits the game when the exit button is pressed.
     */
    public void exitButtonPressed() {
        System.exit(0);
    }

    /**
     * The method that runs when an animal card button is pressed.
     *
     * @param card : the card that is pressed.
     */
    public void animalCardButtonPressed(AnimalCard card) {
        // The method won't run further if the card is not clickable or if it's already chosen.
        if (!card.isClickable() || this.chosenCards.contains(card)) {
            return;
        }
        this.chosenCards.add(card);
        card.setClickable(false);
        card.setIcon(card.getAnimalIcon());
        this.displayTimer.start();
    }

    /**
     * Starts the "delay" timer, and also processes the selected cards.
     */
    public void displayTimerAction() {
        this.displayTimer.stop();
        // If the user has only selected one card.
        if (this.choiceNumber == 1) {
            this.choiceNumber = 2;
            this.mainWindow.getStatusLabel().setText("Pick another card");
            // If the user has already selected two cards.
        } else {
            this.choiceNumber = 1;
            this.numOfTries++;
            this.mainWindow.getTriesNumberLabel().setText(String.valueOf(this.numOfTries));

            // Determines the result of the selected two cards.
            if (this.chosenCards.get(0).hasSameID(this.chosenCards.get(1))) {
                for (AnimalCard chosenCard : this.chosenCards) {
                    chosenCard.setIcon(null);
                    chosenCard.setClickable(false);
                    chosenCard.setVisible(false);
                }

                this.chosenCards.clear();
                this.cardsRemaining -= 2;

                // If there are no cards remaining.
                if (this.cardsRemaining == 0) {
                    this.gameOver = true;
                    this.mainWindow.getStatusLabel().setText("All Matches Found!");
                    this.mainWindow.getStartStopButton().setText("Start Game");
                    this.mainWindow.getExitButton().setEnabled(true);
                } else {
                    this.mainWindow.getStatusLabel().setText("Pick a card");
                }
            } else {
                for (AnimalCard chosenCard : this.chosenCards) {
                    chosenCard.setIcon(AnimalCard.getCover());
                    chosenCard.setClickable(true);
                }
                this.chosenCards.clear();
                this.mainWindow.getStatusLabel().setText("Pick a card");
            }
        }
    }
}